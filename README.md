<!--
Fundamentals of Troubleshooting (c) by Kerrigan Joseph

Fundamentals of Troubleshooting is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
-->

# Fundamentals of Troubleshooting

These fundamentals should apply to troubleshooting in general. Most of the time
we're troubleshooting software, but the same basic principles apply to any
problem within any system.

## Basic Principles

These aren't troubleshooting steps as much as they are general principles to
keep in mind while troubleshooting. Whatever troubleshooting methodology you
use, following these principles will make you better at troubleshooting.

### Slow is smooth, smooth is fast

Don't rush. Slow down and take the time to ask the right questions, gather the
right data, so that you get the right answer. Speeding through the earlier
steps in troubleshooting is a great way to end up drawing the wrong conclusions
without even knowing why.

For issues that impact other people, you will likely be under pressure to solve
the problem quickly. It's important to communicate effectively with whoever that
pressure is coming from, as well as to remain calm and not cut corners or rush
things. Work on engaging with teammates, supervisors, and other stakeholders so
as to get their buy-in on a steady, thorough approach to troubleshooting.

### Assume as little as possible

Note that this is not saying "don't make assumptions". Some assumptions are
necessary, but be very aware of what you are assuming and why. Don't be more
confident in your assumptions than the underlying facts justify, and always be
willing to question your assumptions as new facts arise.

As you do more troubleshooting around any particular system, you'll start to
develop instincts and gut feelings about what could have caused some particular
issue. These instincts are **questions**, _not answers_. They are useful as a
guide to where to focus your investigation, they are _not_ a conclusion in and
of themselves. Let your instincts direct your observations, but don't assume
things on instinct that you have not actually observed.

### Keep track of findings

There will be side-tracks and rabbit-trails as you dig into a problem. One
question will lead to another which will lead to another, so it's important to
remember _why_ you were investigating some particular thing. Document solutions
to individual problems as you resolve them. Someone else will likely have the
same problems sometime in the future, and often that "someone else" is
future-you!

Keep track of this chain of investigation (on paper or digitally), so that you
don't get lost in the weeds. Think of it as a metaphorical "stack", onto which
you "push" new lines of work and "pop" them off as you resolve them. For smaller
issues, you may be able to mentally keep track of this "stack" of investigation,
but writing it down ensures that you don't lose track of what you were doing.

### Be flexible

Keep your mind nimble and be quick to adjust your approach, expectations,
assumptions, etc. in response to changing circumstances. When you get stuck,
respond with a change in approach, not with despair. Real life problems are
messy and generally don't like to follow rules.

Don't be afraid to reach out for help when you get stuck on a problem. Knowing
when you _don't_ know something is as important as knowing what you _do_ know.
Even just having a second set of eyes on a problem can often bring clarity to
what had been a confusing situation. 

## Troubleshooting Steps

These steps roughly map onto the scientific method. I've found that
systematically following these steps in order generally gets me to the right
answer more quickly and reliably than other processes.

### Gather data

The first and most important step in troubleshooting is to accurately and
systematically gather data about the problem, the system(s) the problem is
occurring in, and the circumstances around the problem occurring.

If you gather bad data, you will draw bad conclusions. If you gather incomplete
data, you will make assumptions that are not warranted and end up on the wrong
track.

Some data you gather will raise deeper questions, but stay focused on getting a
complete picture of the data before you begin to investigate. Also keep track
of the questions that the data raises for later investigation. [See: "keep
track of findings"](#keep-track-of-findings).

### Identify affected systems

Narrow down the location of the problem you're troubleshooting as much as
possible. Go wherever the data takes you, and don't go beyond the data you're
finding. Don't assume that a related system is affected unless the data points
you to that conclusion. This will help you avoid wasting time investigating
things that are unrelated to the actual problem. [See: "assume as little as
possible"](#assume-as-little-as-possible).

### Delineate potential causes

The data you gather should allow you to make a list of all of the potential
causes of the problem. There may be very few, or very many potential causes,
but keep track of them all regardless.

A list of causes will often be "tree-like", with categories and subcategories
of causes, and It's usually better to delineate causes "breadth first" rather
than "depth first". This means listing out all of the "top level" categories of
causes first, then going deeper one category at a time and repeating that
process as needed. Doing this will keep you from missing entire categories of
potential causation, which can happen if you take one category of cause and
pursue it fully right from the start.

While it's worth keeping note of the plausibility of each cause, don't reject a
potential cause out-of-hand just because it seems implausible. Let the
experimentation itself lead you to those conclusions. On the flip side, don't
assume that some particularly obvious potential cause must be the answer just
because it seems so plausible. Again, let the experimentation play out. [See:
"assume as little as possible"](#assume-as-little-as-possible).

### Isolate variables

Almost every effect has multiple potential causal factors. Whenever possible,
isolate those factors so that you are only investigating the effect of one at a
time. If you change three things and the problem goes away, you don't know
which of those things (or which combination of them) affected the outcome. It
can feel slower in the moment to change one thing at a time and observe the
outcomes, but it ends up being faster in the long run when you are able to
narrow down the potential cause more effectively. [See: "slow is smooth, smooth
is fast"](#slow-is-smooth-smooth-is-fast).

### Run experiments

Armed with good and complete data, knowledge of which systems are affected, the
potential causes within those systems, and an understanding of what variables
are at play, start changing things and observe the outcomes. If you've done
everything right up to this point, it should be pretty straightforward to zero
in on what the root problem actually is, fix it, and ultimately test that the
fix is correct.

### Rinse and repeat

If you get stuck on any particular point in this process, back up to the
previous point and do it again.

- If you can't figure out which systems are affected, gather more/better data.
- If you can't seem to isolate the right variables, go back and re-evaluate the
  potential causes.
- If none of your experiments are panning out, look for a variable you are
  missing.
- etc.

If all else fails, slow down, go back to the beginning, and question the
assumptions you made in the first place. It may be the case that you are on a
completely wrong track because of one wrong assumption at the very beginning.
[See: "be flexible"](#be-flexible).

### Document the solution

When you finally solve the problem, write everything down somewhere where
others can access it. Who knows what future troubleshooting will benefit from
the specifics of the problem you just solved. It's incredibly frustrating to go
through an entire troubleshooting cycle only to find out that the same kind of
problem was solved a week ago, and nobody wrote it down. [See: "keep track of
findings"](#keep-track-of-findings).

